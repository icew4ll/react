import React from "react"

interface Item {
  url: string
  title: string
  items?: Item[]
}

interface Toc {
  items: Item[]
}

export const Toc = ({ items }: Toc) => {
  // let words = ["Hello", "World", "How are you?"]
  return (
    <ul>
      {items.map(({ url, title }, index) => (
        <li>{index}{url} {title}</li>
      ))}
    </ul>
  )
}

// export const Toc = ({ items }: Toc) => {
//   // const { url, title } = items[0]
//   // return <>{url}{title}</>
//   return items.map(step => <Step {...step}/>);
// }
//
// const Step = () => {
//   const { url, title } =
//   return (
//
//   )
// }
