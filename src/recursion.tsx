import React from "react"

interface Item {
  url: string
  title: string
  items?: Item[]
}

interface Toc {
  items?: Item[]
}

export const Tree = ({ items }: Toc) => {
  if (!items || !items.length) {
    return null
  }
  return (
    <ul>
      {items.map(({ url, title, items }) => (
        <React.Fragment key={url}>
          <li>{title}</li>
          <Tree items={items} />
        </React.Fragment>
      ))}
    </ul>
  )
}
