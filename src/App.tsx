import React, { useState } from "react"
import styled from 'styled-components'
import { Tree } from "./recursion"
import toc from "./test"

const Button = styled.button`
background-color: #5e35b1
`

const clickMe = () => {
  alert("Clicked")
}

const ButtonToggle = styled(Button)`
opacity: 0.7
${({ active }) =>
active &&
`opacity: 1
`}
`

const types = ["Cash", "Credit Card", "Bitcoin"]

const ToggleGroup = () => {
  const [active, setActive] = useState(types[0])
  return (
    <div>
      {types.map(type => (
        <ButtonToggle
          active={active === type}
          onClick={() => setActive(type)}
        >{type}</ButtonToggle>
      ))}
    </div>
  )
}

export default () => {
  console.log(JSON.stringify(toc))
  const { items, text } = toc
  console.log(JSON.stringify(items))
  let t2 = items
  return (
    <>
      <div className="flex fixed w-full items-center justify-between px-6 h-10 bg-white text-gray-700 border-b border-gray-200 z-10">
        <Button onClick={clickMe}>Button</Button>
      </div>
      <div className="pt-10">
        <ToggleGroup />
        <div className="lg:visible invisible fixed right-0 m-2 text-2xl text-blue-500">
          <Tree items={t2} />
        </div>
        <div className="max-w-screen-md bg-gray-800">
          <p>{text}</p>
        </div>
      </div>
    </>
  )
}
